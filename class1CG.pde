float displacement, angle;

void setup() {
  
  size(800, 800, P3D);
  
  // color of the stroke
  stroke(255);
  
  // don't fill up geometry
  noFill();
  
  displacement = 0;
  angle = 0;
}

// similar to update
void draw() {
  // things you will need
  
  // get the cosine of a value
  cos(10);
  
  // get how much time has passed since the begining of execution
  millis();
  
  // set black background
  background(0);
  
  // identity 
  PMatrix3D identity = new PMatrix3D(1, 0, 0, 0,
                                     0, 1, 0, 0,
                                     0, 0, 1, 0,
                                     0, 0, 0, 1);
  
  // translation matrix
  displacement += 1;
  float tx = width/2;
  float ty = height/2;
  float tz = 0;
  PMatrix3D translation = new PMatrix3D(1, 0, 0, tx,
                                        0, 1, 0, ty,
                                        0, 0, 1, tz,
                                        0, 0, 0, 1);
                                        
  PMatrix3D displacementForRotation = new PMatrix3D(1, 0, 0, 200,
                                                    0, 1, 0, 0,
                                                    0, 0, 1, 0,
                                                    0, 0, 0, 1);
                  
                                       
  // millis() - how much time has passed since running the application (in milliseconds)
  float sx = cos(millis() * 0.001);
  float sy = cos(millis() * 0.001);
  float sz = 1;
  PMatrix3D scale = new PMatrix3D(sx, 0,  0,  0,
                                  0,  sy, 0,  0,
                                  0,  0,  sz, 0,
                                  0,  0,  0,  1);
                                        
  
  angle += 0.05f;
  float cosAngle = cos(angle);
  float sinAngle = sin(angle);
// rotation in Z (yaw)
  PMatrix3D rotationZ = new PMatrix3D(cosAngle, -sinAngle, 0, 0,
                                      sinAngle, cosAngle,  0, 0,
                                      0,        0,         1, 0,
                                      0,        0,         0, 1);


  // ANOTHER HINT FOR HOMEWORK - declare all your matrices here 

  // our working matrix
  PMatrix3D result = new PMatrix3D(identity);
  
  // modify my result matrix
  // HINT FOR HOMEWORK - apply all your matrices here
  
  // NOTES REGARDING ORDER - 
  // 1. ORDER IS SUPER IMPORTANT AND REMEMBER IS NOT COMMUTATIVE
  // 2. ORDER IS REVERSED IN CODE

  //REMEMBER! - last operation is the first!
  result.apply(translation);

  
  // rotating along a different point
  result.apply(rotationZ);
  result.apply(displacementForRotation);
  
  // rotating along its own axis
  result.apply(rotationZ);
  
  result.apply(scale);
  
  
  
  // applyMatrix to the world
  applyMatrix(result);
  
  // drawing a square
  beginShape();
  vertex(-50, -50);
  vertex(50, -50);
  vertex(50, 50);
  vertex(-50, 50);
  endShape(CLOSE);
}
